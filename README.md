# ics-ans-role-flycapture

Ansible role to install [FlyCapture SDK](https://www.flir.com/products/flycapture-sdk/).

## Role Variables

```yaml
flycapture_packages:
  - flycap
  - flycapture-doc
  - updatorgui
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-flycapture
```

## License

BSD 2-clause
