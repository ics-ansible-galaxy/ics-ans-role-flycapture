import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("package", ["flycap", "updatorgui"])
def test_flycpature_installed(host, package):
    assert host.package(package).is_installed
